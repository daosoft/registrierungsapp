﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Security;
using System.Web.UI;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace Sportcenter.Pages
{
    public partial class Login : Page
    {
        SqlConnection connection;

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Registrierung_Click(object sender, EventArgs e)
        {
            Response.Redirect("Registration/Registration.aspx");
        }

        protected void Login_Click(object sender, EventArgs e)
        {
            if (AuthenticateUser(TXT_Name.Text,TXT_PW.Text))
            {
                FormsAuthentication.RedirectFromLoginPage(TXT_Name.Text, false);
            }
            else
            {
                lblError.Text = "Ungültiger Benutzername und/oder Passwort!";
            }
        }

        private bool AuthenticateUser(string username, string password)
        {
            using (connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SportcenterConnectionString"].ConnectionString))
            {
                connection.Open();
                SqlCommand cmd = new SqlCommand("spAuthenticateUser", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };

                SqlParameter paramUsername = new SqlParameter("@UserName", username);
                SqlParameter paramPassword = new SqlParameter("@Password", password);

                cmd.Parameters.Add(paramUsername);
                cmd.Parameters.Add(paramPassword);

                int ReturnCode = (int)cmd.ExecuteScalar();
                return ReturnCode == 1;
            }
        }
    }
}