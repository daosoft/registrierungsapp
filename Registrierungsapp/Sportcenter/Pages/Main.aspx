﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Main.aspx.cs" Inherits="Sportcenter.Pages.Main" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../Stylesheets/main.css" rel="stylesheet" />
</head>
<body>
    <form id="menu_form" runat="server">
        <div id="menu">
            <asp:Table ID="MenuTable" runat="server">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="HomeButton" runat="server" Text="Home" OnClick="HomeButton_Click" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Button ID="ReserveButton" runat="server" Text="Reservieren" OnClick="ReserveButton_Click" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Button ID="ContactButton" runat="server" Text="Kontakt" OnClick="ContactButton_Click"/>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Button ID="LogoutButton" runat="server" Text="Logout" OnClick="LogoutButton_Click"/>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Button ID="AdminButton" runat="server" Text="Verwaltung" Visible="false" OnClick="AdminButton_Click"/>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </div>
    </form>
    <div id ="main">
        <p><asp:Label runat="server" ID="lblHeader" Font-Bold="True" Font-Size="Larger"></asp:Label></p>
        <p><asp:Label runat="server" ID="lblName"></asp:Label></p>
        <p><asp:Label runat="server" ID="lblEmail"></asp:Label></p>
        <p><asp:Label runat="server" ID="lblNumber"></asp:Label></p>
    </div>
</body>
</html>