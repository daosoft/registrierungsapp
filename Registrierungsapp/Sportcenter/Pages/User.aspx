﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="User.aspx.cs" Inherits="Sportcenter.Pages.User" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../Stylesheets/admin.css" rel="stylesheet" />
</head>
<body>
    <form id="main_form" runat="server">
        <div id="main_container">
            <ul>
                <li>
                    <asp:Label runat="server" ID="lblError" ForeColor="Red"></asp:Label>
                </li>
                <li>
                    <asp:Button runat="server" ID="ReturnButton" Text="Zurück zur Platzwahl" OnClick="ReturnButton_Click"/>
                </li>
                <li>
                    <asp:GridView ID="ReservationView" runat="server" AutoGenerateColumns="False" DataSourceID="SQLdata" OnRowCommand="Fields_RowCommand">
                        <Columns>
                            <asp:TemplateField HeaderText="Stornieren">
                                <ItemTemplate>
                                    <asp:Button ID="ChooseButton" text="Auswählen" runat="server" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>"/>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="ReservationId" HeaderText="ID" SortExpression="ReservationID" />
                            <asp:BoundField DataField="ResDate" HeaderText="Datum" SortExpression="ResDate" />
                            <asp:BoundField DataField="ResFrom" HeaderText="Von" SortExpression="ResFrom" />
                            <asp:BoundField DataField="ResTo" HeaderText="Bis" SortExpression="ResTo" />
                            <asp:BoundField DataField="FieldName" HeaderText="Platz" />
                        </Columns>
                    </asp:GridView>
                </li>
                <li>
                    <asp:SqlDataSource runat="server" ID="SQLdata" ConnectionString="<%$ ConnectionStrings:SportcenterConnectionString %>" SelectCommand="spShowUserReservations" SelectCommandType="StoredProcedure">
                        <SelectParameters>
                            <asp:QueryStringParameter Name="UserName" QueryStringField="Name" Type="String" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </li>
            </ul>
        </div>
    </form>
    <footer><asp:Label runat="server" ID="lblFooter"></asp:Label></footer>
</body>
</html>
