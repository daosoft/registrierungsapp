﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FieldTimeslots.aspx.cs" Inherits="Sportcenter.Pages.FieldTimeslots" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../Stylesheets/timeslots.css" rel="stylesheet" />
</head>
<body>
    <header><asp:Label runat="server" ID="lblHeader"></asp:Label></header>
    <form id="main_form" runat="server">
        <div id="main_container">
            <asp:Button runat="server" ID="ReturnButton" Text="Zurück zur Feldauswahl" OnClick="ReturnButton_Click"/>
            <div>
                <asp:Label runat="server" ID="lblCalenderInput" Text="Wählen Sie ein Datum: "></asp:Label>
            </div>
            <div id="left_container">
                <asp:Calendar ID="FieldCalender" runat="server" OnSelectionChanged="Calender_SelectionChanged"></asp:Calendar>
            </div>
            <div id="right_container">
                <asp:Table runat="server" ID="TableFieldTimes" BorderStyle="Solid" BorderWidth="1px" Visible="false"></asp:Table>
                <ul>
                    <li><asp:Label runat="server" ID="lblStart" Visible="false"></asp:Label></li>
                    <li><asp:TextBox runat="server" ID="txtStart" Visible="false"></asp:TextBox></li>
                    <li><asp:Label runat="server" ID="lblEnd" Visible="false"></asp:Label></li>
                    <li><asp:TextBox runat="server" ID="txtEnd" Visible="false"></asp:TextBox></li>
                    <li><asp:Button runat="server" ID="ReserveButton" Text="Reservieren" OnClick="ReserveButton_Click" CssClass="ButtonClass" Visible="false"/></li>
                </ul>        
            </div>
        </div>
        <asp:Label runat="server" ID="lblError" ForeColor="Red"></asp:Label>
    </form>
    <footer><asp:Label runat="server" ID="lblFooter"></asp:Label></footer>
</body>
</html>