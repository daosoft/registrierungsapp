﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Admin.aspx.cs" Inherits="Sportcenter.Pages.Admin" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../Stylesheets/admin.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="main" runat="server">
        <div id ="mainContainer">
            <asp:Button runat="server" ID="ReturnButton" Width="100%" Height="60px" BackColor="Lavender" BorderColor="Lavender" OnClick="ReturnButton_Click" Text="Zurück zum Hauptmenü" Font-Bold="false" Font-Size="Large"/>
            <div id="left">
                <ul>
                    <li><asp:Button ID="AddTimeslot" runat="server" Text="Timeslot hinzufügen" OnClick="AddTimeslot_Click" CssClass="buttonClass"/></li>
                    <li><asp:Button ID="DelTimeslot" runat="server" Text="Timeslot entfernen" CssClass="buttonClass" OnClick="DelTimeslot_Click"/></li>
                    <li><asp:Button ID="CloseField"  runat="server" Text="Feld sperren" CssClass="buttonClass" OnClick="CloseField_Click"/></li>
                    <li><asp:Button ID="NewField" runat="server" Text="Neues Feld" CssClass="buttonClass" OnClick="AddField_Click"/></li>
                    <li><asp:Button ID="DelField" runat="server" Text="Feld löschen" CssClass="buttonClass" OnClick="DeleteField_Click" /></li>
                    <li><asp:Button ID="ChangeField" runat="server" Text="Feld ändern" CssClass="buttonClass" OnClick="ChangeField_Click"/></li>
                    <li><asp:Button ID="AddAdmin" runat="server" Text="Admin hinzufügen" CssClass="buttonClass" OnClick="AddAdmin_Click"/></li>
                    <li><asp:Button ID="DelAdmin" runat="server" Text="Admin entfernen" CssClass="buttonClass" OnClick="RemoveAdmin_Click"/></li>
                    <li><asp:Button ID="DelUser" runat="server" Text="Benutzer entfernen" OnClick="DelUser_Click" CssClass="buttonClass"/></li>
                </ul>
            </div>
            <div id="right">
                <ul>
                    <li><asp:Label Visible="false" ID="lblInput" runat="server" CssClass="labelClass"></asp:Label>
                        <asp:DropDownList Visible="false" ID="FieldNameDropDown" runat="server" DataSourceID="SqlDataSource1" DataTextField="FieldName" DataValueField="FieldName">
                        </asp:DropDownList>
                        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:SportcenterConnectionString %>" SelectCommand="SELECT [FieldName] FROM [Field]"></asp:SqlDataSource>
                        <asp:DropDownList Visible="false" ID="UserNameDropDown" runat="server" DataSourceID="SqlDataSource2" DataTextField="UserName" DataValueField="UserName">
                        </asp:DropDownList>
                        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:SportcenterConnectionString %>" SelectCommand="SELECT [UserName] FROM [Users]"></asp:SqlDataSource>
                    </li>
                    <li><asp:TextBox Visible="false" ID="txtInput" runat="server" CssClass="textboxClass"></asp:TextBox></li>
                    <li><asp:Label Visible="false" ID="lblInput2" runat="server" CssClass="labelClass"></asp:Label></li>
                    <li><asp:TextBox Visible="false" ID="txtInput2" runat="server" CssClass="textboxClass"></asp:TextBox></li>
                    <li><asp:Label Visible="false" ID="lblInput3" runat="server" CssClass="labelClass"></asp:Label></li>
                    <li><asp:TextBox Visible="false" ID="txtInput3" runat="server" CssClass="textboxClass"></asp:TextBox></li>
                    <li><asp:Label Visible="false" ID="lblInput4" runat="server" CssClass="labelClass"></asp:Label></li>
                    <li><asp:TextBox Visible="false" ID="txtInput4" runat="server" CssClass="textboxClass"></asp:TextBox></li>
                    <li><asp:Button Visible="false" ID="ExecuteButton" Width= "25%" runat="server" Text="Ausführen" CssClass="buttonClass" OnClick="ExecuteButton_Click"/></li>
                                      <li><asp:Label ID="lblError" runat="server" CssClass="labelClass" ForeColor="Red"></asp:Label></li>
                </ul>
            </div>
        </div>
    </form>
</body>
</html>
