﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Sportcenter.Pages.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../Stylesheets/login.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <div id="main">
            <h1 class="newStyle1">Willkommen im Sportcenter!</h1>
            <div id="first">
                        <asp:Table ID="MainMenu" runat="server" Height="50px" Width="150px" BorderStyle="Groove" BorderColor="Black">
                <asp:TableRow>
                    <asp:TableHeaderCell>
                        <asp:Label ID="UN_Label" runat="server" Text="Benutzername"></asp:Label>
                    </asp:TableHeaderCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableHeaderCell>
                        <asp:TextBox runat="server" ID="TXT_Name"></asp:TextBox>
                    </asp:TableHeaderCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableHeaderCell>
                        <asp:Label ID="PW_Label" runat="server" Text="Passwort"></asp:Label>
                    </asp:TableHeaderCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableHeaderCell>
                        <asp:TextBox runat="server" ID="TXT_PW" TextMode="Password"></asp:TextBox>
                    </asp:TableHeaderCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableHeaderCell>
                         <asp:Button ID="LoginButton" runat="server" Text="Einloggen" Width="100%" Height="100%" OnClick="Login_Click" />
                    </asp:TableHeaderCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableHeaderCell>
                        <asp:Button ID="RegButton" runat="server" Text="Registrieren" Width="100%" Height="100%" OnClick="Registrierung_Click" />
                    </asp:TableHeaderCell>
                </asp:TableRow>
            </asp:Table>
            </div>
        </div>
        <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red"></asp:Label>
    </form>
</body>
</html>
