﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Drawing;
using System.Collections.Generic;
using Sportcenter.Classes;

namespace Sportcenter.Pages
{
    public partial class FieldTimeslots : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                string fieldname = Request.QueryString["Name"];
                lblHeader.Text = "Wählen sie ein Datum und Zeiten zu denen Sie den Platz " + fieldname + " reservieren möchten!";
                lblFooter.Text = "Sie sind momentan als " + HttpContext.Current.User.Identity.Name + " angemeldet.";
                FieldCalender.SelectedDate = DateTime.Today;

                TableItemStyle tableStyle = new TableItemStyle();
                tableStyle.HorizontalAlign = HorizontalAlign.Center;
                tableStyle.VerticalAlign = VerticalAlign.Middle;
                tableStyle.Width = Unit.Pixel(100);
                tableStyle.BackColor = Color.Green;
                Session["Style"] = tableStyle;
                ConstructTimeslotTable();
            }
        }

        protected void Calender_SelectionChanged(object sender, EventArgs e)
        {
            ConstructTimeslotTable();
        }

        protected void ReserveButton_Click(object sender, EventArgs e)
        {
            lblError.Text = "";
            if(IsValidTime(txtStart.Text) && IsValidTime(txtEnd.Text))
            {
                Reservation reservation = new Reservation(int.Parse(txtStart.Text), int.Parse(txtEnd.Text));
                if (reservation.NotZero())
                {
                    if (IsReservationValid(reservation))
                    {
                        AddReservation(reservation);
                        lblError.Text = "Ihre Reservierung wurde Erfolgreich hinzugefügt! (" + FieldCalender.SelectedDate.ToShortDateString() + " Von " +
                            txtStart.Text + " Bis " + txtEnd.Text + ")";
                    }
                    else
                    {
                        lblError.Text = "Bitte wählen Sie einen Zeitraum der noch nicht reserviert ist!";
                    }
                }
                else
                {
                    lblError.Text = "Bitte reservieren Sie mindestens eine Stunde!";
                }
            }
            else
            {
                lblError.Text = "Bitte geben Sie eine gültige Start und/oder Endzeit an!";
            }
            ConstructTimeslotTable();
        }

        protected void ReturnButton_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Pages/Fields.aspx");
        }

        private bool IsValidTime(string input, out int time)
        {
            if (int.TryParse(input, out time))
            {
                if (time >= (int)Session["Start"] && time <= (int)Session["End"])
                    return true;
                else
                    return false;
            }
            else
                return false;
        }

        private bool IsValidTime(string input)
        {
            if (int.TryParse(input, out int time))
            {
                if (time >= (int)Session["Start"] && time <= (int)Session["End"])
                    return true;
                else
                    return false;
            }
            else
                return false;
        }

        private bool IsReservationValid(Reservation current)
        {
            bool sucess = true;
            List<Reservation> reservations = (List<Reservation>)Session["Reservations"];
            foreach (Reservation res in reservations)
            {
                if (current.Includes(res.StartDate))
                    sucess = false;
                if (current.Includes(res.EndDate))
                    sucess = false;
                if (current.IsBetween(res.StartDate, res.EndDate))
                    sucess = false;
            }
            return sucess;
        }

        private List<Reservation> GetReservations()
        {
            string fieldname = Request.QueryString["Name"];
            string date = FieldCalender.SelectedDate.ToString("yyyy-MM-dd");
            List<Reservation> reservations = new List<Reservation>();

            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SportcenterConnectionString"].ConnectionString))
            {
                connection.Open();
                SqlCommand cmd = new SqlCommand("spGetReservationTimes", connection);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramDate = new SqlParameter("@Date", date);
                SqlParameter paramField = new SqlParameter("@FieldName", fieldname);

                cmd.Parameters.Add(paramDate);
                cmd.Parameters.Add(paramField);

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {   
                    while(reader.Read())
                    {
                        reservations.Add(new Reservation((int)reader["ResFrom"],(int)reader["ResTo"]));
                    }
                }
            }
            return reservations;
        }

        private DateTime GetDateTimes(out int starttime, out int endtime)
        {
            string fieldname = Request.QueryString["Name"];
            string cs = ConfigurationManager.ConnectionStrings["SportcenterConnectionString"].ConnectionString;
            DateTime valid; List<Reservation> reservations = new List<Reservation>();
            using (SqlConnection connection = new SqlConnection(cs))
            {
                SqlCommand com = new SqlCommand("spGetValidTimeslot", connection);
                com.CommandType = CommandType.StoredProcedure;
                SqlParameter tablenameParam = new SqlParameter("@FieldName", fieldname);
                com.Parameters.Add(tablenameParam);
                connection.Open();

                using (SqlDataReader reader = com.ExecuteReader())
                {
                    reader.Read();
                    starttime = (int)reader["StartTime"];
                    endtime = (int)reader["EndTime"];
                    valid = (DateTime)reader["ValidUntil"];
                }
            }
            return valid;
        }

        private void AddReservation(Reservation reservation)
        {
            string cs = ConfigurationManager.ConnectionStrings["SportcenterConnectionString"].ConnectionString;
            using (SqlConnection connection = new SqlConnection(cs))
            {
                SqlCommand com = new SqlCommand("spReserveField", connection);
                com.CommandType = CommandType.StoredProcedure;

                SqlParameter paramStart = new SqlParameter("@StartTime", reservation.StartDate);
                SqlParameter paramEnd = new SqlParameter("@EndTime", reservation.EndDate);
                SqlParameter paramField = new SqlParameter("@FieldName", Request.QueryString["Name"]);
                SqlParameter paramUser = new SqlParameter("@UserName", HttpContext.Current.User.Identity.Name);
                SqlParameter paramDate = new SqlParameter("@ResDate", FieldCalender.SelectedDate.ToString("yyyy-MM-dd"));

                com.Parameters.Add(paramStart);
                com.Parameters.Add(paramEnd);
                com.Parameters.Add(paramField);
                com.Parameters.Add(paramUser);
                com.Parameters.Add(paramDate);

                connection.Open();
                com.ExecuteScalar();
            }
        }
        private void ConstructTimeslotTable()
        {
            TableFieldTimes.Visible = false;
            ReserveButton.Visible = false;
            string fieldname = Request.QueryString["Name"];
            DateTime valid = GetDateTimes(out int starttime, out int endtime);
            List<Reservation> reservations = new List<Reservation>();
            reservations = GetReservations();
            TableFieldTimes.Caption = "Datum: " + FieldCalender.SelectedDate.ToShortDateString();
            if (FieldCalender.SelectedDate >= DateTime.Today)
            {
                if (FieldCalender.SelectedDate <= valid)
                {
                    lblStart.Visible = true;
                    lblEnd.Visible = true;
                    txtStart.Visible = true;
                    txtEnd.Visible = true;
                    TableFieldTimes.Visible = true;
                    ReserveButton.Visible = true;
                    Session["Reservations"] = reservations;
                    for (int count = starttime; count < endtime; count++)
                    {
                        TableRow trow = new TableRow();
                        TableFieldTimes.Rows.Add(trow);
                        TableCell tcell = new TableCell();
                        tcell.Text = $"{count}:00 - {count + 1}:00";
                        trow.Cells.Add(tcell);
                    }
                    foreach (TableRow rw in TableFieldTimes.Rows)
                        foreach (TableCell cel in rw.Cells)
                            cel.ApplyStyle((TableItemStyle)Session["Style"]);
                    foreach (Reservation res in reservations)
                        for (int count = res.StartDate; count < res.EndDate; count++)
                        {
                            TableFieldTimes.Rows[count - starttime].Cells[0].BackColor = Color.Red;
                        }
                    lblStart.Text = "Von (" + starttime + "-" + endtime + ")";
                    lblEnd.Text = "Bis (" + starttime + "-" + endtime + ")";
                }
                else
                {
                    lblError.Text = "Für dieses Datum sind noch keine Zeitfenster freigeschalten!\n Versuchen Sie eine früheres Datum!";
                }
            }
            else
            {
                lblStart.Visible = false;
                lblEnd.Visible = false;
                txtStart.Visible = false;
                txtEnd.Visible = false;
                if (reservations.Count != 0)
                {
                    TableFieldTimes.Visible = true;
                    foreach (Reservation res in reservations)
                    {
                        TableRow trow = new TableRow();
                        TableFieldTimes.Rows.Add(trow);
                        TableCell tcell = new TableCell();
                        tcell.Text = $"Reservierung: {res.StartDate} - {res.EndDate}";
                        trow.Cells.Add(tcell);
                    }
                    foreach (TableRow rw in TableFieldTimes.Rows)
                        foreach (TableCell cel in rw.Cells)
                            cel.ApplyStyle((TableItemStyle)Session["Style"]);
                }

            }
            Session["Timetable"] = TableFieldTimes;
            if (FieldCalender.SelectedDate.Date == DateTime.Today)
                starttime = DateTime.Now.Hour + 1;
            Session["Start"] = starttime;
            Session["End"] = endtime;
        }
    }
}