﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Web.Security;
using System.Data.SqlClient;
using System.Configuration;

namespace Sportcenter.Pages
{
    public partial class Main : Page
    {
        SqlConnection connection;

        protected void Page_Load(object sender, EventArgs e)
        {
            lblHeader.Text = "Willkommen im Sportcenter " + HttpContext.Current.User.Identity.Name + "!";

            if (CheckAdmin(HttpContext.Current.User.Identity.Name))
            {
                AdminButton.Visible = true;
            }
        }

        public bool CheckAdmin(string username)
        {
            using (connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SportcenterConnectionString"].ConnectionString))
            {
                connection.Open();
                SqlCommand cmd = new SqlCommand("spCheckAdmin", connection);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramUsername = new SqlParameter("@UserName", username);

                cmd.Parameters.Add(paramUsername);

                int ReturnCode = (int)cmd.ExecuteScalar();
                return ReturnCode == 1;
            }
        }

        protected void AdminButton_Click(object sender, EventArgs e)
        {
            if (CheckAdmin(HttpContext.Current.User.Identity.Name))
            {
                Response.Redirect("Admin.aspx");
            }
        }

        protected void LogoutButton_Click(object sender, EventArgs e)
        {
            FormsAuthentication.SignOut();
            Response.Redirect("Login.aspx");
        }

        protected void ContactButton_Click(object sender, EventArgs e)
        {
            lblHeader.Text = "Kontaktdaten";
            lblName.Text = "Dragan Runjaic";
            lblEmail.Text = "dado.runjaic@gmail.com";
            lblNumber.Text = "0664//5142302";
        }

        protected void HomeButton_Click(object sender, EventArgs e)
        {
            lblHeader.Text = "Willkommen im Sportcenter " + HttpContext.Current.User.Identity.Name + "!";
            lblName.Text = "";
            lblEmail.Text = "";
            lblNumber.Text = "";
        }

        protected void ReserveButton_Click(object sender, EventArgs e)
        {
            Response.Redirect("Fields.aspx");
        }
    }
}