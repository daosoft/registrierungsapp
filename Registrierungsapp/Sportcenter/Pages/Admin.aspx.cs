﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Web.Security;
using System.Data.SqlClient;
using System.Configuration;
using System.Threading.Tasks;
using Sportcenter.Classes;

namespace Sportcenter.Pages
{
    public partial class Admin : Page
    {
        SqlConnection connection;
        enum Command { Time_add, Time_remove, Field_deny, User_del, Add_Admin, Remove_Admin, Add_Field, Change_Field, Del_Field };

        protected void Page_Load(object sender, EventArgs e)
        {
            // Do something maybe?
        }

        protected void AddTimeslot_Click(object sender, EventArgs e)
        {
            lblError.Text = "";
            Session["Command"] = Command.Time_add;
            UserNameDropDown.Visible = false;
            FieldNameDropDown.Visible = false;
            lblInput.Visible = true;
            txtInput.Visible = true;
            lblInput2.Visible = true;
            txtInput2.Visible = true;
            lblInput3.Visible = true;
            txtInput3.Visible = true;
            lblInput4.Visible = true;
            txtInput4.Visible = true;
            ExecuteButton.Visible = true;
            lblInput.Text = "Bennenen Sie ihren Timeslot:";
            lblInput2.Text = "Geben Sie eine Startzeit ein (0-24)";
            lblInput3.Text = "Geben Sie eine Endzeit ein (0-24)";
            lblInput4.Text = "Geben Sie die Gültigkeitsdauer an (YYYY-MM-DD)";
        }

        protected void DelTimeslot_Click(object sender, EventArgs e)
        {
            lblError.Text = "";
            Session["Command"] = Command.Time_remove;
            UserNameDropDown.Visible = false;
            FieldNameDropDown.Visible = false;
            lblInput.Visible = true;
            txtInput.Visible = true;
            lblInput2.Visible = false;
            txtInput2.Visible = false;
            lblInput3.Visible = false;
            txtInput3.Visible = false;
            lblInput4.Visible = false;
            txtInput4.Visible = false;
            ExecuteButton.Visible = true;
            lblInput.Text = "Wählen Sie einen Timeslot-Namen:";
        }

        protected void DelUser_Click(object sender, EventArgs e)
        {
            lblError.Text = "";
            Session["Command"] = Command.User_del;
            UserNameDropDown.Visible = false;
            FieldNameDropDown.Visible = false;
            lblInput.Visible = true;
            txtInput.Visible = true;
            lblInput2.Visible = false;
            txtInput2.Visible = false;
            lblInput3.Visible = false;
            txtInput3.Visible = false;
            lblInput4.Visible = false;
            txtInput4.Visible = false;
            ExecuteButton.Visible = true;
            lblInput.Text = "Geben Sie einen Benutzernamen ein:";
        }

        protected void CloseField_Click(object sender, EventArgs e)
        {
            lblError.Text = "";
            Session["Command"] = Command.Field_deny;
            UserNameDropDown.Visible = false;
            FieldNameDropDown.Visible = true;
            lblInput.Visible = true;
            txtInput.Visible = false;
            lblInput2.Visible = false;
            txtInput2.Visible = false;
            lblInput3.Visible = false;
            txtInput3.Visible = false;
            lblInput4.Visible = false;
            txtInput4.Visible = false;
            ExecuteButton.Visible = true;
            lblInput.Text = "Wählen Sie ein Feld (Name)";
        }
        protected void ChangeField_Click(object sender, EventArgs e)
        {
            lblError.Text = "";
            Session["Command"] = Command.Change_Field;
            UserNameDropDown.Visible = false;
            FieldNameDropDown.Visible = true;
            lblInput.Visible = true;
            txtInput.Visible = false;
            lblInput2.Visible = true;
            txtInput2.Visible = true;
            lblInput3.Visible = true;
            txtInput3.Visible = true;
            lblInput4.Visible = true;
            txtInput4.Visible = true;
            ExecuteButton.Visible = true;
            lblInput.Text = "Wählen Sie ein Feld (Name)";
            lblInput2.Text = "Hier können Sie den Namen des Feldes ändern!";       
            lblInput3.Text = "Ist das Feld derzeit zugänglich? Schreiben Sie 'Y' für Ja oder 'N' für Nein!";
            lblInput4.Text = "Hier können Sie den Typ des Feldes ändern!";

        }
        protected void AddAdmin_Click(object sender, EventArgs e)
        {
            lblError.Text = "";
            Session["Command"] = Command.Add_Admin;
            UserNameDropDown.Visible = true;
            FieldNameDropDown.Visible = false;
            lblInput.Visible = true;
            txtInput.Visible = false;
            lblInput2.Visible = false;
            txtInput2.Visible = false;
            lblInput3.Visible = false;
            txtInput3.Visible = false;
            lblInput4.Visible = false;
            txtInput4.Visible = false;
            ExecuteButton.Visible = true;
            lblInput.Text = "Wählen Sie einen Benutzer (Name)";
        }
        protected void RemoveAdmin_Click(object sender, EventArgs e)
        {
            lblError.Text = "";
            Session["Command"] = Command.Remove_Admin;
            UserNameDropDown.Visible = true;
            FieldNameDropDown.Visible = false;
            lblInput.Visible = true;
            txtInput.Visible = false;
            lblInput2.Visible = false;
            txtInput2.Visible = false;
            lblInput3.Visible = false;
            txtInput3.Visible = false;
            lblInput4.Visible = false;
            txtInput4.Visible = false;
            ExecuteButton.Visible = true;
            lblInput.Text = "Wählen Sie einen Benutzer (Name)";
        }
        protected void AddField_Click(object sender, EventArgs e)
        {
            lblError.Text = "";
            Session["Command"] = Command.Add_Field;
            UserNameDropDown.Visible = false;
            FieldNameDropDown.Visible = false;
            lblInput.Visible = true;
            txtInput.Visible = true;
            lblInput2.Visible = true;
            txtInput2.Visible = true;
            lblInput3.Visible = false;
            txtInput3.Visible = false;
            lblInput4.Visible = false;
            txtInput4.Visible = false;
            ExecuteButton.Visible = true;
            lblInput.Text = "Name des neuen Feldes:";
            lblInput2.Text = "Typ des neues Feldes (Hallenplatz oder Außenplatz):";
        }
        protected void DeleteField_Click(object sender, EventArgs e)
        {
            lblError.Text = "";
            Session["Command"] = Command.Del_Field;
            UserNameDropDown.Visible = false;
            FieldNameDropDown.Visible = true;
            lblInput.Visible = true;
            txtInput.Visible = false;
            lblInput2.Visible = false;
            txtInput2.Visible = false;
            lblInput3.Visible = false;
            txtInput3.Visible = false;
            lblInput4.Visible = false;
            txtInput4.Visible = false;
            ExecuteButton.Visible = true;
            lblInput.Text = "Name des neuen Feldes:";

        }

        protected void ExecuteButton_Click(object sender, EventArgs e)
        {
            switch ((Command)Session["Command"])
            {
                case Command.User_del:
                    if (_DelUser(txtInput.Text) == false)
                    {
                        lblError.Text = "Please enter a valid Username!";
                    }
                    else
                    {
                        lblError.Text = $"User {txtInput.Text} has been removed!";
                    }
                    break;
                case Command.Time_remove:
                    if (_DelTimeslot(txtInput.Text) == false)
                    {
                        lblError.Text = "Please enter a valid Timeslot-Name!";
                    }
                    else
                    {
                        lblError.Text = $"Timeslot {txtInput.Text} has been removed!";
                    }
                    break;
                case Command.Time_add:
                    if ((IsValidTime(txtInput2.Text) == true) && (IsValidTime(txtInput3.Text) == true) && (IsValidDate(txtInput4.Text)))
                    {
                        if (AddTimeSlot(txtInput.Text, txtInput2.Text, txtInput3.Text, txtInput4.Text))
                        {
                            lblError.Text = $"Timeslot {txtInput.Text} has been added!";
                        }
                        else
                        {
                            lblError.Text = $"Timeslot {txtInput.Text} already exists!";
                        }
                    }
                    else
                    {
                        lblError.Text = "Invalid timeformat!";
                    }

                    break;
                case Command.Field_deny:
                    if (BlockField(FieldNameDropDown.Text, false) == true)
                    {
                        lblError.Text = $"{FieldNameDropDown.Text} is no longer available!";
                    }
                    else
                    {
                        lblError.Text = $"Field {FieldNameDropDown.Text} does not exist!";
                    }
                    break;
                case Command.Add_Admin:
                    if (_AddAdmin(UserNameDropDown.Text) == true)
                    {
                        MessageBox.Show($"{UserNameDropDown.Text} ist nun Admin!");

                    }
                    else
                    {
                        lblError.Text = $"Fehler!";
                    }
                    break;
                case Command.Remove_Admin:
                    if (_RemoveAdmin(UserNameDropDown.Text) == true)
                    {
                        MessageBox.Show($"{UserNameDropDown.Text} ist nun kein Admin mehr!");

                    }
                    else
                    {
                        lblError.Text = $"Fehler!";
                    }
                    break;
                case Command.Add_Field:
                    if (_AddField(txtInput.Text, txtInput2.Text) == true)
                    {
                        MessageBox.Show($"Sie haben erfolgreich das Feld: {txtInput.Text} hinzugefügt");
                    }
                    else
                    {
                        lblError.Text = "Fehler!";
                    }
                    break;
                case Command.Change_Field:
                    if (txtInput2.Text != "")
                    {
                        ChangeFieldName(FieldNameDropDown.Text, txtInput2.Text);
                        MessageBox.Show("Sie haben soeben den Namen des Feldes geändert!");
                    }
                    if (txtInput3.Text != "" && txtInput3.Text == "Y")
                    {
                        bool acess = false; 
                        UnblockField(FieldNameDropDown.Text, acess);
                        MessageBox.Show($"Sie haben soeben den Zugang zu Feld {FieldNameDropDown.Text} freigeschalten!");
                    }
                    else if (txtInput3.Text != "" && txtInput3.Text == "N")
                    {
                        bool acess = true;
                        BlockField(FieldNameDropDown.Text, acess);
                        MessageBox.Show($"Sie haben soeben den Zugang zu Feld {FieldNameDropDown.Text} blockiert!");
                    }
                    if (txtInput4.Text != "")
                    {
                        ChangeFieldType(FieldNameDropDown.Text, txtInput4.Text);
                        MessageBox.Show($"Sie haben soeben den Typ des Feldes {FieldNameDropDown.Text} auf {txtInput4.Text} geändert!");
                    }
                    else
                    {
                        lblError.Text = "Sie haben keine gültigen Daten eingetragen!";
                    }
                   
                    break;
                case Command.Del_Field:
                     if (_DelField(FieldNameDropDown.Text)== true)
                    {
                        MessageBox.Show($"Sie haben soeben das Feld: {FieldNameDropDown.Text} aus der Datenbank entfernt!");
                    }
                     else
                    {
                        lblError.Text = "Fehler beim Löschen!";
                    }
                    break;
                default:
                    lblError.Text = "Command not defined!";
                    break;
            }
        }

     

        protected void ReturnButton_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Pages/Main.aspx");
        }
        private bool _DelField(string fieldName)
        {
            using (connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SportcenterConnectionString"].ConnectionString))
            {
                connection.Open();

                SqlCommand cmd = new SqlCommand("spDelField", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };

                SqlParameter paramFieldName = new SqlParameter("@FieldName", fieldName);

                cmd.Parameters.Add(paramFieldName);
                int ReturnCode = (int)cmd.ExecuteScalar();
                return ReturnCode == 1;


            }
        }
        private bool _AddField(string fieldName, string fieldType)
        {
            using (connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SportcenterConnectionString"].ConnectionString))
            {
                connection.Open();

                SqlCommand cmd = new SqlCommand("spAddField", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };

                SqlParameter paramFieldName = new SqlParameter("@FieldName", fieldName);
                SqlParameter paramFieldTypeID = new SqlParameter("@FieldType", fieldType);

                cmd.Parameters.Add(paramFieldName);
                cmd.Parameters.Add(paramFieldTypeID);


                int ReturnCode = (int)cmd.ExecuteScalar();
                return ReturnCode == 1;


            }
        }
        private bool ChangeFieldName(string fieldName, string newFieldName)
        {
            using (connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SportcenterConnectionString"].ConnectionString))
            {
                connection.Open();

                SqlCommand cmd = new SqlCommand("spChangeFieldName", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };

                SqlParameter paramFieldName = new SqlParameter("@FieldName", fieldName);
                SqlParameter paramnewFieldName = new SqlParameter("@NewFieldName", newFieldName);

                cmd.Parameters.Add(paramFieldName);
                cmd.Parameters.Add(paramnewFieldName);

                int ReturnCode = (int)cmd.ExecuteScalar();
                return ReturnCode == 1;


            }
        }
        private bool ChangeFieldType (string fieldName, string newFieldType)
        {
            using (connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SportcenterConnectionString"].ConnectionString))
            {
                connection.Open();

                SqlCommand cmd = new SqlCommand("spChangeFieldType", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };

                SqlParameter paramFieldName = new SqlParameter("@FieldName", fieldName);
                SqlParameter paramnewFieldType = new SqlParameter("@NewFieldType", newFieldType);

                cmd.Parameters.Add(paramFieldName);
                cmd.Parameters.Add(paramnewFieldType);

                int ReturnCode = (int)cmd.ExecuteScalar();
                return ReturnCode == 1;


            }
        }

        private bool _RemoveAdmin(string username)
        {
            using (connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SportcenterConnectionString"].ConnectionString))
            {
                connection.Open();
                SqlCommand cmd = new SqlCommand("spRemoveAdmin", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };

                SqlParameter paramUsername = new SqlParameter("@UserName", username);

                cmd.Parameters.Add(paramUsername);

                int ReturnCode = (int)cmd.ExecuteScalar();
                return ReturnCode == 1;
            }
        }
        private bool _AddAdmin(string username)
        {
            using (connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SportcenterConnectionString"].ConnectionString))
            {
                connection.Open();
                SqlCommand cmd = new SqlCommand("spAddAdmin", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };

                SqlParameter paramUsername = new SqlParameter("@UserName", username);

                cmd.Parameters.Add(paramUsername);

                int ReturnCode = (int)cmd.ExecuteScalar();
                return ReturnCode == 1;
            }
        }
        private bool _DelUser(string username)
        {
            using (connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SportcenterConnectionString"].ConnectionString))
            {
                connection.Open();
                SqlCommand cmd = new SqlCommand("spDelUser", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };

                SqlParameter paramUsername = new SqlParameter("@UserName", username);

                cmd.Parameters.Add(paramUsername);

                int ReturnCode = (int)cmd.ExecuteScalar();
                return ReturnCode == 1;
            }
        }

        private bool _DelTimeslot(string name)
        {
            using (connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SportcenterConnectionString"].ConnectionString))
            {
                connection.Open();
                SqlCommand cmd = new SqlCommand("spDelTimeslot", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };

                SqlParameter paramUsername = new SqlParameter("@Name", name);

                cmd.Parameters.Add(paramUsername);

                int ReturnCode = (int)cmd.ExecuteScalar();
                return ReturnCode == 1;
            }
        }

        private bool AddTimeSlot(string name, string start, string end, string validUntil)
        {
            using (connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SportcenterConnectionString"].ConnectionString))
            {
                connection.Open();
                SqlCommand cmd = new SqlCommand("spAddTime", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };

                SqlParameter paramUsername = new SqlParameter("@Name", name);
                SqlParameter paramStart = new SqlParameter("@StartTime", start);
                SqlParameter paramEnd = new SqlParameter("@EndTime", end);
                SqlParameter paramValid = new SqlParameter("@ValidUntil", validUntil);

                cmd.Parameters.Add(paramUsername);
                cmd.Parameters.Add(paramStart);
                cmd.Parameters.Add(paramEnd);
                cmd.Parameters.Add(paramValid);

                int ReturnCode = (int)cmd.ExecuteScalar();
                return ReturnCode == 1;
            }
        }

        private bool UnblockField(string fieldname, bool acess)
        {
            string acess_str;
            if (acess == false)
                acess_str = "N";
            else
                acess_str = "Y";

            using (connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SportcenterConnectionString"].ConnectionString))
            {
                connection.Open();
                SqlCommand cmd = new SqlCommand("spDisableField", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };

                SqlParameter paramUsername = new SqlParameter("@Name", fieldname);
                SqlParameter paramBool = new SqlParameter("@Bool", acess_str);

                cmd.Parameters.Add(paramUsername);
                cmd.Parameters.Add(paramBool);

                int ReturnCode = (int)cmd.ExecuteScalar();
                return ReturnCode == 1;
            }
        }

        private bool BlockField(string fieldname, bool acess)
        {
            string acess_str;
            if (acess == true)
                acess_str = "Y";
            else
                acess_str = "N";

            using (connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SportcenterConnectionString"].ConnectionString))
            {
                connection.Open();
                SqlCommand cmd = new SqlCommand("spDisableField", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };

                SqlParameter paramUsername = new SqlParameter("@Name", fieldname);
                SqlParameter paramBool = new SqlParameter("@Bool", acess_str);

                cmd.Parameters.Add(paramUsername);
                cmd.Parameters.Add(paramBool);

                int ReturnCode = (int)cmd.ExecuteScalar();
                return ReturnCode == 1;
            }
        }

        static bool IsValidTime(string input)
        {
            if (int.TryParse(input, out int time))
            {
                if (time >= 0 && time <= 24)
                    return true;
                else
                    return false;
            }
            else
                return false;
        }

        static bool IsValidDate(string input)
        {
            return DateTime.TryParse(input, out DateTime dummyOutput);
        }


    }
}