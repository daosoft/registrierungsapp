﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Registration.aspx.cs" Inherits="Sportcenter.Pages.Registration" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../../Stylesheets/registration.css" rel="stylesheet" />
    <script>
        function PWValidation()
        {
            if (document.getElementById("PW").value != document.getElementById("PW_confirm").value)
            {
                alert("Passwörter stimmen nicht überein!");
            }
            if (document.getElementById("PW").value == "" && document.getElementById("PW_confirm").value == "")
            {
                alert("Bitte geben Sie ein Passwort ein!");
            }
        }
    </script>
</head>
<body>
    <h1 id="header">Tennisplatzregistrierung</h1>
    <form id="main" runat="server">
        <div class="container">
            <label><b>Benutzername</b></label>
            <asp:TextBox ID="Name" runat="server" ToolTip="Geben Sie einen Benutzernamen ein!"></asp:TextBox>
            <label><b>Email</b></label>
            <asp:TextBox ID="Email" runat="server" ToolTip="Geben Sie eine Email Adresse ein!"></asp:TextBox>
            <label><b>Passwort</b></label>
            <asp:TextBox ID="PW" runat="server" ToolTip="Geben Sie ein Passwort ein!" TextMode="Password"></asp:TextBox>
            <label><b>Passwort wiederholen</b></label>
            <asp:TextBox ID="PW_confirm" runat="server" ToolTip="Wiederholen Sie Ihr Passwort!" TextMode="Password"></asp:TextBox>

            <div>
                <asp:Button runat="server" Text="Abbrechen" ID="cancleButton" Width="49%" BackColor="#F44336" OnClick="cancleButton_Click"/>
                <asp:Button runat="server" Text="Registrieren!" ID="registerButton" Width="49%" BackColor="#4CAF50" OnClick="registerButton_Click" OnClientClick="PWValidation()"/>
            </div>
      </div>
    </form>
</body>
</html>