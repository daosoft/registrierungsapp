﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web.UI;
using System.Configuration;
using System.Data.SqlClient;

namespace Sportcenter.Pages
{
    public partial class Registration : Page
    {
        SqlConnection connection;

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void registerButton_Click(object sender, EventArgs e)
        {
            if (PW.Text == PW_confirm.Text && PW.Text != "" && PW_confirm.Text != "")
            {
                using (connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SportcenterConnectionString"].ConnectionString))
                {
                    connection.Open();
                    SqlCommand com = new SqlCommand("spRegisterUser", connection);
                    com.CommandType = CommandType.StoredProcedure;

                    com.Parameters.AddWithValue("@UserName", Name.Text);
                    com.Parameters.AddWithValue("@Email", Email.Text);
                    com.Parameters.AddWithValue("@Password", PW.Text);

                    int ReturnCode = (int)com.ExecuteScalar();
                    if (ReturnCode == -1)
                    {
                        Response.Write("Benutzername existiert bereits, bitte wählen Sie einen neuen Benutzernamen!");
                    }
                    else
                    {
                        Response.Redirect("~/Pages/Login.aspx");
                    }
                }
            }
        }

        protected void cancleButton_Click(object sender, EventArgs e)
        {
            PW.Text = "";
            PW_confirm.Text = "";
            Email.Text = "";
            Name.Text = "";
        }
    }
}