﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

namespace Sportcenter.Pages
{
    public partial class User : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                lblFooter.Text = "Sie sind momentan als " + HttpContext.Current.User.Identity.Name + " angemeldet.";
            }
        }

        protected void ReturnButton_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Pages/Fields.aspx");
        }

        protected void Fields_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int index = Convert.ToInt32(e.CommandArgument);
            GridViewRow row = ReservationView.Rows[index];

            DateTime select = Convert.ToDateTime(row.Cells[2].Text);
            int currentHour = DateTime.Now.Hour;
            int selectedHour = int.Parse(row.Cells[3].Text);
            if ((select == DateTime.Today && selectedHour > currentHour) || select > DateTime.Today)
            {
                DeleteReservation(row.Cells[1].Text);
                Response.Redirect(Request.RawUrl);
            }
            else
            {
                lblError.Text = "Reservierungen in der Vergangenheit können nicht storniert werden!";
            }
        }

        private void DeleteReservation(string id)
        {
            string cs = ConfigurationManager.ConnectionStrings["SportcenterConnectionString"].ConnectionString;
            using (SqlConnection connection = new SqlConnection(cs))
            {
                SqlCommand com = new SqlCommand("spDelReservation", connection);
                com.CommandType = CommandType.StoredProcedure;

                SqlParameter paramID = new SqlParameter("@ResID", id);

                com.Parameters.Add(paramID);

                connection.Open();
                com.ExecuteScalar();
            }            
        }
    }
}