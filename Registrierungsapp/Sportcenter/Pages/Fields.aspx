﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Fields.aspx.cs" Inherits="Sportcenter.Pages.Fields" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../Stylesheets/Fields.css" rel="stylesheet" type="text/css"/>
</head>
<body>
    <header>Reservierungen - Wählen Sie einen Platz!</header>
    <form id="main_form" runat="server">
        <div id="main_container">
            <ul>
                <li>
                    <asp:Button runat="server" ID="ReturnButton" OnClick="ReturnButton_Click" Text="Zurück zum Hauptmenü"/>
                </li>
                <li>
                    <asp:Button runat="server" ID="MyReservations" Text="Meine Reservierungen" OnClick="MyReservations_Click" />
                </li>
                <li>
                    <asp:GridView ID="FieldGrid" runat="server" AutoGenerateColumns="False" DataSourceID="SportcenterDataSource" OnRowCommand="Fields_RowCommand">
                <Columns>
                    <asp:TemplateField HeaderText="Platzwahl">
                        <ItemTemplate>
                            <asp:Button ID="ChooseButton" text="Auswählen" runat="server" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>"/>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="FieldName" HeaderText="Name" SortExpression="Name" ReadOnly="true"/>
                    <asp:BoundField DataField="TimeslotName" HeaderText="Zeitfenster" SortExpression="ActiveTimeslot" ReadOnly="true"/>
                    <asp:BoundField DataField="FieldTypeName" HeaderText="Platztyp" SortExpression="FieldTypeID" ReadOnly="true"/>
                </Columns>
            </asp:GridView>
                </li>
            </ul>
            
            <asp:SqlDataSource ID="SportcenterDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:SportcenterConnectionString %>" 
                SelectCommand=
                    "SELECT Field.FieldName, Timeslots.TimeslotName, Field_Type.FieldTypeName
	                FROM ((Field
	                INNER JOIN Field_Type ON Field.FieldTypeID = Field_Type.FieldTypeId)
	                Inner JOIN Timeslots ON Field.ActiveTimeslot = Timeslots.TimeslotId)
	                WHERE ([Accessible] = 'Y')">  
            </asp:SqlDataSource>
        </div>
    </form>
    <footer><asp:Label runat="server" ID="lblFooter"></asp:Label></footer>
</body>
</html>
