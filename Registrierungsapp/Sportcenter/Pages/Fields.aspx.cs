﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sportcenter.Pages
{
    public partial class Fields : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            lblFooter.Text = "Sie sind momentan als " + HttpContext.Current.User.Identity.Name + " angemeldet.";
        }

        protected void Fields_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int index = Convert.ToInt32(e.CommandArgument);
            GridViewRow row = FieldGrid.Rows[index];
            
            Response.Redirect("~/Pages/FieldTimeslots.aspx?Name=" + row.Cells[1].Text);
        }

        protected void ReturnButton_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Pages/Main.aspx");
        }

        protected void MyReservations_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Pages/User.aspx?Name=" + HttpContext.Current.User.Identity.Name);
        }
    }
}