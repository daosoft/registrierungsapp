﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sportcenter.Classes
{
    public class Reservation
    {
        public int StartDate;
        public int EndDate;

        public Reservation(int startDate, int endDate)
        {
            StartDate = startDate;
            EndDate = endDate;
        }

        public bool Includes(int input)
        {
            if (input > StartDate && input < EndDate)
                return true;
            else
                return false;
        }

        public bool IsBetween(int start, int end)
        {
            if (StartDate > start && StartDate < end)
                return true;
            else
                return false;
        }

        public bool NotZero()
        {
            if (StartDate == EndDate)
                return false;
            else
                return true;
        }
    }
}