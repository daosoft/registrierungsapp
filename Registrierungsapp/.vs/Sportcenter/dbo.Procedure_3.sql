﻿CREATE PROCEDURE [dbo].[_spAddAdmin]
	@UserName varchar (50)

AS
Begin
	Declare @count int
	Declare @ReturnCode int

	select @count = count(UserName) from Users
	where [UserName] = @UserName
	If(@Count = 1)
	Begin
		Update Users SET [IsAdmin] = 'Y' 
		WHERE [UserName] = @UserName
		Set @ReturnCode = 1
	End
	Else
	Begin
		Set @ReturnCode = -1
	End
	select @ReturnCode as ReturnValue
End 