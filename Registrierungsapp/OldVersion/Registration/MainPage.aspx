﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MainPage.aspx.cs" Inherits="Registration.MainPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="main.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <div id="menu">
            <ul>
                <li><asp:HyperLink ID="Home" runat="server" CssClass="menu_link">Home</asp:HyperLink></li>
                <li><asp:HyperLink ID="Reservation" runat="server" CssClass="menu_link">Reservieren</asp:HyperLink></li>
                <li><asp:HyperLink ID="Registration" runat="server" CssClass="menu_link" NavigateUrl="~/Registration.aspx">Registrieren</asp:HyperLink></li>
                <li><asp:HyperLink ID="Contact" runat="server" CssClass="menu_link">Kontakt</asp:HyperLink></li>
                <li><asp:HyperLink ID="Admin" runat="server" CssClass="menu_link">Verwaltung</asp:HyperLink></li>
            </ul>
        </div>
        <div>
            Test
        </div>
    </form>
</body>
</html>
