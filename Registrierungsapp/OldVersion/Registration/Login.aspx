﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Registration.Main" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        #form1 {
            width: 150px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Table ID="MainMenu" runat="server" Height="50px" Width="150px" BorderStyle="Groove" BorderColor="Black" Caption="Login">
                <asp:TableRow>
                    <asp:TableHeaderCell>
                        <asp:Label ID="UN_Label" runat="server" Text="Email"></asp:Label>
                    </asp:TableHeaderCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableHeaderCell>
                        <asp:TextBox runat="server" ID="TXT_Email"></asp:TextBox>
                    </asp:TableHeaderCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableHeaderCell>
                        <asp:Label ID="PW_Label" runat="server" Text="Passwort"></asp:Label>
                    </asp:TableHeaderCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableHeaderCell>
                        <asp:TextBox runat="server" ID="TXT_PW" TextMode="Password"></asp:TextBox>
                    </asp:TableHeaderCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableHeaderCell>
                         <asp:Button ID="Login" runat="server" Text="Einloggen" Width="100%" Height="100%" OnClick="Login_Click" />
                    </asp:TableHeaderCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableHeaderCell>
                        <asp:Button ID="Registrierung" runat="server" Text="Registrieren" Width="100%" Height="100%" OnClick="Registrierung_Click" />
                    </asp:TableHeaderCell>
                </asp:TableRow>
            </asp:Table>
        </div>
    </form>
</body>
</html>
