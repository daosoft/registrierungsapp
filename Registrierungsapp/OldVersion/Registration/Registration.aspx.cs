﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;

namespace Registration
{
    public partial class Registration : Page
    {
        SqlConnection connection;

        protected void Page_Load(object sender, EventArgs e)
        {
            if(IsPostBack)
            {
                //todo
            }
        }

        protected void registerButton_Click(object sender, EventArgs e)
        {
            if (PW.Text == PW_confirm.Text && PW.Text != "" && PW_confirm.Text != "")
            {
                using (connection = new SqlConnection(ConfigurationManager.ConnectionStrings["RegistrationConnectionString"].ConnectionString))
                {
                    connection.Open();
                    string insertQuery = "insert into Users (Email,Password) values (@email, @pw)";
                    SqlCommand com = new SqlCommand(insertQuery, connection);
                    com.Parameters.AddWithValue("@email", Email.Text);
                    com.Parameters.AddWithValue("@pw", PW.Text);
                    com.ExecuteNonQuery();
                    Response.Redirect("Manager.aspx");
                }
            }
        }

        protected void cancleButton_Click(object sender, EventArgs e)
        {
            PW.Text = "";
            PW_confirm.Text = "";
            Email.Text = "";
        }
    }
}